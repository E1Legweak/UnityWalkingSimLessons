using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitchScript : MonoBehaviour
{
    public TriggerScript trigger;
    public MouseInteractScript interactScript;
    public Light spotLight;
    public Animation anim;
    public AudioSource sound;

    private bool isOn;

    // Update is called once per frame
    void Update()
    {
        LightSwitch();
    }

    void LightSwitch() 
    {
        if (trigger.triggerEntered == true && interactScript.clickable == true) 
        {
            if (Input.GetMouseButtonDown(0)) 
            {
                if (isOn == true)
                {
                    spotLight.intensity = 0;
                    anim.Play();
                    sound.Play();
                    isOn = false;
                }
                else 
                {
                    spotLight.intensity = 3.5f;
                    anim.Play(); 
                    sound.Play();
                    isOn = true;
                }
                
            }
        }
    }
}
