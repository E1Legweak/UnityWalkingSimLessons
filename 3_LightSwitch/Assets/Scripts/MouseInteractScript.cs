using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInteractScript : MonoBehaviour
{
    public bool clickable;

    private void OnMouseEnter()
    {
        clickable = true;
        Debug.Log("MouseEnter");
    }

    private void OnMouseExit()
    {
        clickable = false;
        Debug.Log("MouseExit");
    }

}
