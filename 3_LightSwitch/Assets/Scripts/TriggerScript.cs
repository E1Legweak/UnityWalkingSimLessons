using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public bool triggerEntered;

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player") 
        {
            triggerEntered = true;
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            triggerEntered = false;
        }
    }
}
