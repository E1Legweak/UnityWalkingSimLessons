using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInteractionScript : MonoBehaviour
{
    public bool mouseInteract = false;


    private void OnMouseEnter()
    {
        mouseInteract = true;
        Debug.Log("mouse interact = " + mouseInteract);
    }

    private void OnMouseExit()
    {
        mouseInteract = false;
        Debug.Log("mouse interact = " + mouseInteract);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
