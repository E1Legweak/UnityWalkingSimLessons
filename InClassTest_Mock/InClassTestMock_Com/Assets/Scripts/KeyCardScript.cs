using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCardScript : MonoBehaviour
{
    public TriggerScript trigger;
    public MouseInteractionScript interaction;
    public Collider col;
    public Renderer rend;
    public bool hasKeyCard = false;
    AudioSource sound;

    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        PickUp();
    }

    void PickUp() 
    {
        if (trigger.inTrigger == true && interaction.mouseInteract == true) 
        {
            if (Input.GetMouseButtonDown(0)) 
            {
                sound.Play();
                hasKeyCard = true;
                col.enabled = false;
                rend.enabled = false;
            }
        }
    }
}
