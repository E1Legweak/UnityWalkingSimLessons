using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLight : MonoBehaviour
{
    Light candleLight;

    float nextLightChange = 0;

    // Start is called before the first frame update
    void Start()
    {
        candleLight = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (nextLightChange < Time.time) 
        {
            SetIntensity();
            nextLightChange = Time.time + Random.Range(0.04f, 0.12f);
        }
    }

    void SetIntensity() 
    {
        candleLight.intensity = Random.Range(0.5f, 2f);
    }
}
