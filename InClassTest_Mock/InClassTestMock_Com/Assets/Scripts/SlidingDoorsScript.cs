using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoorsScript : MonoBehaviour
{
    public TriggerScript trigger;
    public MouseInteractionScript interaction;
    public GameObject door_01;
    public GameObject door_02;
    public float openAmount = 1.2f;
    public AudioSource buttonSound;

    AudioSource sound;

    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        OpenDoor();   
    }

    void OpenDoor() 
    {
        if (trigger.inTrigger == true && interaction.mouseInteract == true) 
        {
            if (Input.GetMouseButtonDown(0)) 
            {
                sound.Play();
                buttonSound.Play();
                door_01.transform.localPosition = new Vector3(openAmount, 0, 0);
                door_02.transform.localPosition = new Vector3(-openAmount, 0, 0);
            }
        }
    }
}
