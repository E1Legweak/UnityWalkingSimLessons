using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingDoorScript : MonoBehaviour
{
    public GameObject door;
    public KeyScript key;
    public float openAngle = 120;
    public float rotationSpeed = 5f;
    public float snap = 0.01f;

    public AudioClip openSound;
    public AudioClip lockedSound;
    
    public TriggerScript interactionTrigger;
    public TriggerScript collisionTrigger;

    MouseInteractScript interactScript;
    float doorDot = 0;
    float targetAngle = 0;
    GameObject player;
    bool doorRunning = false;
    float lastFrameAngle = 0;
    AudioSource sound;

    enum DoorState {closed, openTo, openAway, partOpenTo, partOpenAway};
    DoorState doorState;
    

    // Start is called before the first frame update
    void Start()
    {
        interactScript = GetComponentInChildren<MouseInteractScript>();
        player = GameObject.FindGameObjectWithTag("Player");
        sound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInteraction();
    }

    void CheckInteraction() 
    {
        if (interactionTrigger.inTrigger && interactScript.mouseInteract == true)
        {

            if (Input.GetMouseButtonDown(0) == true)
            {
                if (key == null || key.hasKey == true)
                {
                    if (doorRunning == true)
                    {
                        doorRunning = false;
                        StopCoroutine("DoorMovement");
                        SetClosedCheck();
                        //sound.Stop();
                    }
                    else
                    {
                        SetTargetAngle();
                        StartCoroutine("DoorMovement");
                        sound.Stop();
                        sound.clip = openSound;
                        sound.pitch = Random.Range(0.8f, 1.2f);
                        sound.Play();
                        //DoorMotion();
                    }
                }
                else
                {
                    sound.Stop();
                    sound.clip = lockedSound;
                    sound.pitch = Random.Range(0.8f, 1.2f);
                    sound.Play();
                }
            }
            
        }
    }

    void SetTargetAngle()
    {
        doorDot = Vector3.Dot(player.transform.position - door.transform.position, door.transform.forward);
        //Debug.Log(doorDot);

        switch (doorState)
        {
            case DoorState.closed:
                if (doorDot >= 0)
                {
                    targetAngle = -openAngle;
                    doorState = DoorState.openAway;
                }
                else
                {
                    targetAngle = openAngle;
                    doorState = DoorState.openTo;
                }
                break;

            case DoorState.partOpenTo:
                if (doorDot >= 0)
                {
                    targetAngle = 0;
                    doorState = DoorState.closed;
                }
                else
                {
                    targetAngle = openAngle;
                    doorState = DoorState.openTo;
                }
                break;
            case DoorState.partOpenAway:
                if (doorDot >= 0)
                {
                    targetAngle = -openAngle;
                    doorState = DoorState.openAway;
                }
                else
                {
                    targetAngle = 0;
                    doorState = DoorState.closed;
                }
                break;
            default:
                targetAngle = 0;
                break;
        }
    }

    void DoorMotion() 
    {
        door.transform.localEulerAngles = new Vector3(0, targetAngle, 0);
    }

    IEnumerator DoorMovement() 
    {
        doorRunning = true;
        Quaternion targetRot = Quaternion.Euler(new Vector3(0, targetAngle, 0));

        while (Quaternion.Angle(door.transform.localRotation, targetRot) >= snap)
        {
            if (collisionTrigger.inTrigger == true) 
            {
                StopCheck();
            }
            lastFrameAngle = door.transform.localEulerAngles.y;
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, targetRot, rotationSpeed * Time.deltaTime);
            yield return null;
        }

        doorRunning = false;
        door.transform.localRotation = targetRot;
        lastFrameAngle = door.transform.localEulerAngles.y;
        SetClosedCheck();
        yield return null;
    }

    void StopCheck() 
    {
        float doorDot = Vector3.Dot(player.transform.position - door.transform.position, door.transform.forward);
        if (doorDot <= 0 && doorState == DoorState.openTo)
        {
            StopCoroutine("DoorMovement");
            door.transform.localEulerAngles = new Vector3(0, lastFrameAngle, 0);
            doorState = DoorState.partOpenTo;
        }
        else if (doorDot >= 0 && doorState == DoorState.openAway)
        {
            StopCoroutine("DoorMovement");
            door.transform.localEulerAngles = new Vector3(0, lastFrameAngle, 0);
            doorState = DoorState.partOpenAway; 
        }
        SetClosedCheck();
        doorRunning = false;
    }

    void SetClosedCheck() 
    {
        sound.Stop();
        if (Mathf.Abs(door.transform.localEulerAngles.y) <= 20)
        {
            doorState = DoorState.closed;
        }
        else if (Mathf.Abs(door.transform.localEulerAngles.y) >= 340) 
        {
            doorState = DoorState.closed;
        }
    }
}
