using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInteractScript : MonoBehaviour
{
    public bool mouseInteract = false;
    private void OnMouseEnter()
    {
        mouseInteract = true;
    }

    private void OnMouseExit()
    {
        mouseInteract = false;
    }
}
