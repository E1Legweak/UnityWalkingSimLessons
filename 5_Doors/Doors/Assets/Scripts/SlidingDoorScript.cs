using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoorScript : MonoBehaviour
{
    //public variables
    //door references
    public GameObject door_1;
    public GameObject door_2;
    //door motion
    public float openAmount = 1;
    public float moveSpeed = 1;
    public float snap = 0.01f;
    
    //private variables
    //references
    AudioSource doorSound;
    TriggerScript trigger;
    //value variables
    float targetX = 0;
    float doorX = 0;

    // Start is called before the first frame update
    void Start()
    {
        //assign references from elswhere in the prefab
        doorSound = GetComponent<AudioSource>();
        trigger = GetComponentInChildren<TriggerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        TargetSet();
        DoorMotion();
    }

    //changes the door target x location based on the player entering the trigger and exiting the trigger
    void TargetSet() 
    {
        if (trigger.inTrigger == true)
        {
            targetX = openAmount;
        }
        else 
        {
            targetX = 0;
        }
    }

    //moves the doors to the target x locations
    void DoorMotion() 
    {
        if (Mathf.Abs(targetX - door_1.transform.localPosition.x) > snap)
        {
            if (doorSound.isPlaying == false) 
            {
                doorSound.Play();
            }
            doorX = Mathf.Lerp(door_1.transform.localPosition.x, targetX, moveSpeed * Time.deltaTime);
            door_1.transform.localPosition = new Vector3(doorX, 0, 0);
            door_2.transform.localPosition = new Vector3(-doorX, 0, 0);
        }
        else 
        {
            doorSound.Stop();
            door_1.transform.localPosition = new Vector3(targetX, 0, 0);
            door_2.transform.localPosition = new Vector3(-targetX, 0, 0);
        }
    }
}
