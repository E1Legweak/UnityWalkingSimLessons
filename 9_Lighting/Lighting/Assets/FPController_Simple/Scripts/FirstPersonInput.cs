using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonInput : MonoBehaviour
{
    public Vector2 moveVector;
    public Vector2 lookVector;
    public bool isSprinting;

    //public GameObject objectInteract;

    enum PlayerState {normal, interacting};
    PlayerState playerState = PlayerState.normal;

    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        StateSwitch();
        StateMachine();
    }


    public void StateSwitch() 
    {
        if (Input.GetKeyDown(KeyCode.K)) 
        {
            if (playerState == PlayerState.normal)
            {
                playerState = PlayerState.interacting;
            }
            else 
            {
                playerState = PlayerState.normal;
            }
        }
    }

    void StateMachine() 
    {
        switch (playerState)
        {
            case PlayerState.normal:
                Sprinting();
                MovementInput();
                LookInput();
                break;
            case PlayerState.interacting:
                Vector3 rotateVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                //objectInteract.transform.RotateAround(objectInteract.transform.position, rotateVector, 45);

                break;
            default:
                Debug.LogError("Imposible player state");
                break;
        }
    }

    void MovementInput() 
    {
        moveVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    void Sprinting() 
    {
        if (Input.GetKeyDown(KeyCode.LeftShift)) 
        {
            isSprinting = true;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isSprinting = false;
        }
    }

    void LookInput() 
    {
        lookVector = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
    }
}
