using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public TriggerScript trigger;
    public Transform target;
    FirstPersonMovement pMove;
    FirstPersonLook pLook;

    // Start is called before the first frame update
    void Start()
    {
        pMove = FindObjectOfType<FirstPersonMovement>();
        pLook = FindObjectOfType<FirstPersonLook>();
    }

    // Update is called once per frame
    void Update()
    {
        RunTeleport();
    }

    void RunTeleport() 
    {
        if (trigger.inTrigger == true) 
        {
            pMove.enabled = false;
            pLook.enabled = false;
            pMove.transform.position = target.position;
            pMove.transform.rotation = target.rotation;
            pLook.ZeroVelocity();
            pMove.enabled = true;
            pLook.enabled = true;
        }
    }
    /*
     * This needs adding to the first person look script
     * 
    public void ZeroVelocity()
    {
        velocity = Vector2.zero;
        frameVelocity = Vector2.zero;
    }
    */
}
