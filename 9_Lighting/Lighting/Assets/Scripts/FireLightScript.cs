using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLightScript : MonoBehaviour
{
    public Light fireLight;

    float flickerTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Flicker();
    }

    void Flicker() 
    {
        if (Time.time > flickerTime) 
        {
            fireLight.intensity = Random.Range(0.8f, 1.4f);
            fireLight.transform.localPosition = new Vector3(Random.Range(-0.01f, 0.01f), Random.Range(-0.01f, 0.01f), Random.Range(-0.01f, 0.01f));
            flickerTime = Time.time + Random.Range(0.05f, 0.07f);
        }
    }
}
