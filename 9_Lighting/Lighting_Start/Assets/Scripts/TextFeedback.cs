using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextFeedback : MonoBehaviour
{
    public string interactionMsg = "Interactable";
    public TriggerScript trigger;
    public MouseInteractScript interact;

    InteractionTextScript interactionText;

    bool interactable = false;
    bool changed = false;

    // Start is called before the first frame update
    void Start()
    {
        interactionText = FindObjectOfType<InteractionTextScript>();
    }

    void Update()
    {
        Listener();
        Feedback();
    }

    void Listener()
    {
        if (trigger.inTrigger == true && interact.mouseInteract == true)
        {
            interactable = true;
        }
        else
        {
            interactable = false;
        }
    }

    void Feedback()
    {
        if (changed != interactable)
        {
            if (interactable == true)
            {
                interactionText.SetText(interactionMsg);
            }
            else
            {
                interactionText.SetText("");
            }
            changed = interactable;
        }
    }
}
