using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionFeedback : MonoBehaviour
{
    public TriggerScript trigger;
    public MouseInteractScript interact;

    CrossHairScript crossHair;

    bool interactable = false;
    bool changed = false;

    // Start is called before the first frame update
    void Start()
    {
        crossHair = FindObjectOfType<CrossHairScript>();
    }

    // Update is called once per frame
    void Update()
    {
        Listener();
        Feedback();
    }

    void Listener() 
    {
        if (trigger.inTrigger == true && interact.mouseInteract == true)
        {
            interactable = true;
        }
        else 
        {
            interactable = false;
        }
    }

    void Feedback() 
    {
        if (changed != interactable) 
        {
            if (interactable == true)
            {
                crossHair.SetIneractive();
            }
            else 
            {
                crossHair.SetInert();
            }
            changed = interactable;
        }
    }
}
