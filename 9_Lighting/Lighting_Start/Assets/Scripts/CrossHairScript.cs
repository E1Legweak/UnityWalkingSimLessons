using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CrossHairScript : MonoBehaviour
{
    public Image inert;
    public Image interactive;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetIneractive() 
    {
        inert.enabled = false;
        interactive.enabled = true;
    }

    public void SetInert()
    {
        inert.enabled = true;
        interactive.enabled = false;
    }
}
