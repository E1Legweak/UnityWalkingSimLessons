using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InteractionTextScript : MonoBehaviour
{
    public Text interactionText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetText(string msg) 
    {
        interactionText.text = msg;
    }
}
