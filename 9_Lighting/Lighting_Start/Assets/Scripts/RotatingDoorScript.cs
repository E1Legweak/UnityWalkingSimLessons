using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingDoorScript : MonoBehaviour
{
    public GameObject door;
    public TriggerScript trigger;
    public MouseInteractScript interactScript;
    public float openAngle = 120;
    public float rotationSpeed = 5f;
    public float snap = 0.01f;
    public KeyScript key;
    public AudioClip locked;
    public AudioClip open;

    float targetAngle = 0;
    float doorDot = 0;
    GameObject player;

    bool isOpen = false;
    bool isRunning = false;
    AudioSource sound;

    InteractionTextScript interactionText;
    bool interactable = false;
    bool changed = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        sound = GetComponent<AudioSource>();
        interactionText = FindObjectOfType<InteractionTextScript>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInteraction();
        Feedback();
    }

    void CheckInteraction() 
    {
        if (trigger.inTrigger == true && interactScript.mouseInteract == true)
        {
            interactable = true;
            if (Input.GetMouseButtonDown(0))
            {
                if (key == null || key.hasKey == true)
                {
                    if (isRunning == false)
                    {
                        SetTargetAngle();
                    }
                    changed = !changed;
                }
                else
                {
                    interactionText.SetText("Locked");
                    sound.clip = locked;
                    sound.pitch = Random.Range(0.8f, 1.2f);
                    sound.Play();
                }
            }
        }
        else 
        {
            interactable = false;
        }
    }

    void Feedback()
    {
        if (changed != interactable)
        {
            if (interactable == true)
            {
                if (isOpen == true)
                {
                    interactionText.SetText("Close Door");
                }
                else 
                {
                    interactionText.SetText("Open Door");
                }
            }
            else
            {
                interactionText.SetText("");
            }
            changed = interactable;
        }
    }

    void SetTargetAngle() 
    {
        doorDot = Vector3.Dot(player.transform.position - door.transform.position, door.transform.forward);
        Debug.Log(doorDot);

        if (isOpen == true)
        {
            targetAngle = 0;
            isOpen = false;
        }
        else 
        {
            if(doorDot <= 0) 
            {
                targetAngle = openAngle;
            }
            else
            {
                targetAngle = -openAngle;

            }
            isOpen = true;
        }
        StartCoroutine("DoorMovement");
    }

    IEnumerator DoorMovement()
    {
        Quaternion targetRot = Quaternion.Euler(new Vector3(0, targetAngle, 0)); //Sets target as quaternion
        isRunning = true; //Stores state of door moving

        sound.clip = open;
        sound.pitch = Random.Range(0.8f, 1.2f); //Selects random pitch for sound
        sound.Play(); //Plays sound

        while (Quaternion.Angle(door.transform.localRotation, targetRot) >= snap) //Checks if door is not near taget rotation
        {
            //Sets door rotation.
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, targetRot, rotationSpeed * Time.deltaTime);
            yield return null;// Kicks back out to assess while loop on next frame
        }

        //If door is near target rotation, sets rotation to target rotation
        door.transform.localRotation = targetRot;
        isRunning = false; //Stores state of door not moving
        sound.Stop(); //Stops sound
        yield return null; //Ends coroutine
    }
}
