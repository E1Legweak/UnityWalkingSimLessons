﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstSript : MonoBehaviour
{
    bool trueOrFalse = true;
    int wholeNumber = 1;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("This happened");

        ReadVariables();

        ReadVariables();
    }

    void ReadVariables() 
    {
        if (trueOrFalse == true)
        {
            trueOrFalse = false;
        }
        else 
        {
            trueOrFalse = true;
        }

        if (wholeNumber > 0)
        {
            wholeNumber = 0;
        }
        else if (wholeNumber == 0) 
        {
            wholeNumber = 1;
        }

        Debug.Log(trueOrFalse);
        Debug.Log(wholeNumber);
    }

    /*
    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update is being called");
        //MyFirstFunction();
    }*/

    // The first function we have made
    void MyFirstFunction()
    {
        Debug.Log("My function was called");
    }
}
