using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeScript : MonoBehaviour
{
    public TriggerScript bridgeTrigger;
    public MouseInteractScript bridgeInteraction;
    public Animation bridgeAnim;
    public Animation switchAnim;
    public AudioSource bridgeSound;
    public AudioSource switchSound;

    bool isComplete;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        BridgeSwitch();
    }

    void BridgeSwitch() 
    {
        if (bridgeTrigger.inTrigger && bridgeInteraction.mouseInteract) 
        {
            if (Input.GetMouseButtonDown(0) )
            {
                switchAnim.Stop();
                switchAnim.Play();
                switchSound.Play();
                if (isComplete == false)
                {
                    Debug.Log("Move Bridge");
                    bridgeAnim.Play();
                    bridgeSound.Play();
                    isComplete = true;
                }
            }
        }

        if (bridgeAnim.isPlaying == false && bridgeSound.isPlaying == true) 
        {
            bridgeSound.Stop();
        }
    }
}
