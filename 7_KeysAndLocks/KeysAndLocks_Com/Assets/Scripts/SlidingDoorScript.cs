using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoorScript : MonoBehaviour
{
    //public variables
    //door references
    public GameObject door_1;
    public GameObject door_2;
    //door motion
    public float openAmount = 1;
    public float moveSpeed = 1;
    public float snap = 0.01f;
    
    //private variables
    //references
    AudioSource doorSound;
    TriggerScript trigger;
    //value variables
    float targetX = 0;
    float doorX = 0;

    bool triggerLastFrame = false;

    // Start is called before the first frame update
    void Start()
    {
        //assign references from elswhere in the prefab
        doorSound = GetComponent<AudioSource>();
        trigger = GetComponentInChildren<TriggerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        TargetSet();
    }

    //changes the door target x location based on the player entering the trigger and exiting the trigger
    void TargetSet() 
    {
        //Ensures the coroutine is not continously called.
        if(triggerLastFrame != trigger.inTrigger) 
        {
            //Sets target based on whether the player is in the trigger or not
            if (trigger.inTrigger == true)
            {
                targetX = openAmount;
            }
            else
            {
                targetX = 0;
            }

            triggerLastFrame = trigger.inTrigger;
            StopCoroutine("DoorMovement");
            StartCoroutine("DoorMovement");
        }
    }

    //moves the doors to the target x locations
    IEnumerator DoorMovement()
    {
        doorSound.Play(); //Plays sound
        while (Mathf.Abs(targetX - door_1.transform.localPosition.x) > snap) //Checks if door is not near taget rotation
        {
            doorX = Mathf.Lerp(door_1.transform.localPosition.x, targetX, moveSpeed * Time.deltaTime); //Runs lerp

            door_1.transform.localPosition = new Vector3(doorX, 0, 0); //Sets door 1 position to current transition point
            door_2.transform.localPosition = new Vector3(-doorX, 0, 0); //Sets door 2 position to current transition point
            yield return null;// Kicks back out to assess while loop on next frame
        }

        doorSound.Stop(); // Stops Sound
        door_1.transform.localPosition = new Vector3(targetX, 0, 0); //Sets door 1 position to it's target
        door_2.transform.localPosition = new Vector3(-targetX, 0, 0); //Sets door 2 position to it's target
        yield return null; //Ends coroutine
    }
}
