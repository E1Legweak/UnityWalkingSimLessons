﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throw : MonoBehaviour
{
    public float force = 1;
    Rigidbody _rb;
    AudioSource _as;
    bool playedSound;


    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _as = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void ScareEvent()
    {
        _rb.useGravity = true;
        _rb.AddForce(transform.forward * force, ForceMode.Impulse);
    }

    void OnCollisionEnter()
    {
        if (playedSound == false)
        {
            _as.Play();
            playedSound = true;
        }
    }
}
