using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitchScript : MonoBehaviour
{
    public TriggerScript trigger;
    public MouseInteractScript interact;
    public Light spotLight;
    public AudioSource sound;
    public Animation anim;

    private bool isOn = false;

    // Update is called once per frame
    void Update()
    {
        LightSwitch();
    }

    void LightSwitch() 
    {
        if (trigger.inTrigger == true && interact.mouseInteract == true) 
        {
            if (Input.GetMouseButtonDown(0))
            {
                sound.Play();
                anim.Stop();
                anim.Play();

                if (isOn == true)
                {
                    spotLight.intensity = 0;
                    isOn = false;
                }
                else 
                {
                    spotLight.intensity = 3.5f;
                    isOn = true;
                }
                
            }
        }
    }
}
