using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StoryManager : MonoBehaviour
{
    public Text storyText;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Message1", 2);
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Message1() 
    {
        storyText.text = "When I arrived, it was quiet...";
        Invoke("Message2", 4);
    }
    void Message2()
    {
        storyText.text = "Where was everyone?";
        Invoke("Message3", 4);
    }
    void Message3()
    {
        storyText.text = "I decided to look around";
        Invoke("LoadScene", 4);
    }

    void LoadScene() 
    {
        SceneManager.LoadScene(1);
    }
}
