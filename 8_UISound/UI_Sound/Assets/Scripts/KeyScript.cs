using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    public TriggerScript trigger;
    public MouseInteractScript interact;
    public Collider col;
    public Renderer rend;
    public bool hasKey = false;

    AudioSource pickUpSound;

    // Start is called before the first frame update
    void Start()
    {
        pickUpSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        PickUp();
    }

    void PickUp() 
    {
        if (trigger.inTrigger == true && interact.mouseInteract == true) 
        {
            if (Input.GetMouseButtonDown(0)) 
            {
                Debug.Log("POOOO!");
                pickUpSound.Play();
                hasKey = true;
                col.enabled = false;
                rend.enabled = false;
            }
        }
    }
}
